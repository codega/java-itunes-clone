package com.experis.iTunesClone.data_access;

import com.experis.iTunesClone.models.TrackFull;

import java.sql.*;
import java.util.ArrayList;

public class TrackSearchData {
    private static Connection connection = null;

    public static ArrayList<TrackFull> searchForTrack(String name){
        //List to add all matching tracks to
        ArrayList<TrackFull> matchingTracks = new ArrayList<>();

        //Query to search for all tracks whose name includes the track being searched for
        //Will include, for example, songs named 'Heartbreak' if you search for 'break'
        String searchQuery = "SELECT track.Name as trackName, artist.Name as artistName, album.Title as albumTitle, genre.Name as genreName ";
        searchQuery += "FROM track NATURAL JOIN album INNER JOIN artist on album.ArtistId = artist.ArtistID INNER JOIN genre on genre.GenreId = track.GenreId ";
        searchQuery += "WHERE UPPER(track.Name) LIKE ('%"+name+"%')";
        try {
            //open connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(searchQuery);
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //Creates 'full' Track objects, adds them to the list
            while (results.next()){
                String trackName = results.getString("trackName");
                String artistName = results.getString("artistName");
                String albumName = results.getString("albumTitle");
                String genreName = results.getString("genreName");

                TrackFull track = new TrackFull(trackName, artistName, albumName, genreName);
                matchingTracks.add(track);
            }

        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        //error handling, if what the user searches for is blank, create an empty list
        if (name == null || name.length() == 0 || name.equals(" ")){
            matchingTracks = new ArrayList<TrackFull>();
        }
        return matchingTracks;
    }
}
