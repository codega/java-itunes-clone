package com.experis.iTunesClone.data_access;

import com.experis.iTunesClone.models.Artist;

import java.sql.*;
import java.util.ArrayList;

public class ArtistData {
    //creates a private connection
    private static Connection connection = null;

    public static ArrayList<Artist> getFiveArtists(){
        //list to store the five artists in
        ArrayList<Artist> artist = new ArrayList<Artist>();

        try {
            //opens connection to the database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            //SQL statement to get five random artists
            PreparedStatement statement = connection.prepareStatement("SELECT ArtistId, Name FROM artist ORDER BY RANDOM() LIMIT 5");
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //adds artists to artist-list
            while (results.next()){
                artist.add(new Artist(results.getString("ArtistId"), results.getString("Name")));
            }
        } catch (SQLException exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return  artist;
    }
}
