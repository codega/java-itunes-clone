package com.experis.iTunesClone.data_access;

import com.experis.iTunesClone.models.Genre;

import java.sql.*;
import java.util.ArrayList;

public class GenreData {
    private static Connection connection = null;

    public static ArrayList<Genre> getFiveGenres(){
        //list to add five random genres to
        ArrayList<Genre> genres = new ArrayList<Genre>();

        try {
            //opens connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            //SQL-statement to get five random genres
            PreparedStatement statement = connection.prepareStatement("SELECT GenreId, Name FROM genre ORDER BY RANDOM() LIMIT 5");
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //adds genres to list
            while (results.next()){
                genres.add(new Genre(results.getString("GenreId"), results.getString("Name")));
            }
        } catch (SQLException exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return genres;
    }

}
