package com.experis.iTunesClone.data_access;

import com.experis.iTunesClone.models.*;

import java.sql.*;
import java.util.ArrayList;

public class CustomerData {
    private static Connection connection = null;

    public static ArrayList<Customer> getAllCustomers(){
        //list to store all customers in
        ArrayList<Customer> allCustomers = new ArrayList<Customer>();
        //query to get all customers
        String customerQuery = "SELECT CustomerId,FirstName,LastName, Email, Country,PostalCode,Phone FROM customer";

        try {
            //open connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(customerQuery);
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //creates Customer objects and adds them to
            while (results.next()){
                String customerID = results.getString("CustomerId");
                String firstName = results.getString("FirstName");
                String lastName = results.getString("LastName");
                String email = results.getString("Email");
                String country = results.getString("Country");
                String postCode = results.getString("PostalCode");
                String phoneNumber = results.getString("Phone");
                Customer newCustomer = new Customer(customerID, firstName, lastName, email, country, postCode, phoneNumber);
                allCustomers.add(newCustomer);
            }

        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return allCustomers;
    }

    public static boolean addNewCustomer(Customer newCustomer){
        boolean added = false;
        try {
            //query to add new customer
            //has "blank" ?-values to be inserted from the customer object given
            String addCustomerQuery = "INSERT INTO customer(FirstName, LastName, Email, Country, PostalCode, Phone, SupportRepId) VALUES(?,?,?,?,?,?, ?)";
            //opens connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(addCustomerQuery);

            //adds the values to statement from the customer object given
            statement.setString(1, newCustomer.getFirstName());
            statement.setString(2, newCustomer.getLastName());
            statement.setString(3, newCustomer.getEmail());
            statement.setString(4, newCustomer.getCountry());
            statement.setString(5, newCustomer.getPostalCode());
            statement.setString(6, newCustomer.getPhoneNumber());
            //hard-coded employee rep
            statement.setString(7, "1");
            //executes update, not query
            statement.executeUpdate();
            //true so that APIController can give correct HTTP-response
            added = true;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
            //false so tha APIController can give correct HTTP-response
            added = false;
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return added;
    }

    public static boolean updateExistingCustomer(String id, Customer customer){
        boolean updated = false;
        try {
            //query to update customer (chosen by id)
            //has 'blank' ?-fields to be updated by information from the customer object provided
            String customerQuery = "UPDATE customer  SET FirstName = ?, LastName = ?, Email = ?, Country = ?, PostalCode = ?, Phone = ? WHERE CustomerId = ?";
            //opens the connection to the database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(customerQuery);
            //fills the blank fields in the query
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getEmail());
            statement.setString(4, customer.getCountry());
            statement.setString(5, customer.getPostalCode());
            statement.setString(6, customer.getPhoneNumber());
            statement.setString(7, id);
            //executes update
            statement.executeUpdate();
            //updates to true so that APIController can give correct HTTP-response
            updated = true;
        } catch (Exception exception){
            System.out.println(exception.getMessage());
            //updates to false so that APIController can give correct HTTP-response
            updated = false;
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return updated;
    }

    public static ArrayList<CustomerByCountry> getCustomersByCountriesDescending(){
        //list to store countries in
        ArrayList<CustomerByCountry> descendingCountryCustomers = new ArrayList<CustomerByCountry>();
        //SQL-query to get countries by how many users they have, in descending order
        String countryQuery = "SELECT Country, Count(CustomerId) as numberOfCustomers FROM customer GROUP BY Country ORDER BY Count(CustomerId) DESC";

        try {
            //opens connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(countryQuery);
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //goes through results and creates Country-objects and adds them to list
            while (results.next()){
                String countryName = results.getString("Country");
                int customerNumber = results.getInt("numberOfCustomers");
                descendingCountryCustomers.add(new CustomerByCountry(countryName, customerNumber));
            }
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return descendingCountryCustomers;
    }

    public static ArrayList<CustomerByProfit> getCustomersByProfitDescending(){
        //list to put customers in
        ArrayList<CustomerByProfit> descendingProfitCustomers = new ArrayList<CustomerByProfit>();
        //SQL-query to get customers and the sum of their total spending by descending order
        String profitQuery = "SELECT CustomerId, FirstName, LastName, SUM(Total) as totalSum FROM customer NATURAL JOIN invoice GROUP BY CustomerId ORDER BY totalSum DESC";

        try {
            //opens connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            PreparedStatement statement = connection.prepareStatement(profitQuery);
            //executes query and stores results
            ResultSet results = statement.executeQuery();

            //Creates CustomerByProfit objects and adds them to list
            while (results.next()) {
                String customerID = results.getString("CustomerId");
                String firstName = results.getString("FirstName");
                String lastName = results.getString("LastName");
                Double spending = results.getDouble("totalSum");
                descendingProfitCustomers.add(new CustomerByProfit(customerID, firstName, lastName, spending));
            }
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return descendingProfitCustomers;
    }

    public static ArrayList<GenreCountTotal> getUserTopGenre(String id){
        //list to add top genres to
        ArrayList<GenreCountTotal> topGenre = new ArrayList<GenreCountTotal>();
        try {
            //opens connection
            connection = DriverManager.getConnection(ConnectionHelper.URL);

            //query to get the max genre count
            String genreQuery = "SELECT genre.GenreId, genre.Name as GenreName, Count(genre.Name) as genreCount";
            genreQuery += " FROM customer NATURAL JOIN invoice NATURAL JOIN invoiceline";
            genreQuery += " INNER JOIN track on invoiceline.TrackId = track.TrackId";
            genreQuery += " INNER JOIN genre on track.GenreId = genre.GenreId";
            genreQuery += " WHERE CustomerId = ?";
            genreQuery += " GROUP BY genre.GenreId";
            genreQuery += " ORDER BY Count(GenreName) DESC";
            genreQuery += " LIMIT 1";

            //executes query, stores results
            PreparedStatement statement = connection.prepareStatement(genreQuery);
            //sets customerID to providedID
            statement.setString(1, id);
            //gets results
            ResultSet results = statement.executeQuery();

            int genreCount = 0;
            //stores the genre max
            while (results.next()) {
                genreCount = results.getInt("genreCount");
            }

            //query to find all genres with max count for the given user
            String findAllTopGenresQuery = "SELECT genre.GenreId, genre.Name as GenreName, Count(genre.Name) as totalNumber";
            findAllTopGenresQuery += " FROM customer NATURAL JOIN invoice NATURAL JOIN invoiceline";
            findAllTopGenresQuery += " INNER JOIN track on invoiceline.TrackId = track.TrackId";
            findAllTopGenresQuery += " INNER JOIN genre on track.GenreId = genre.GenreId";
            findAllTopGenresQuery += " WHERE CustomerId = ?";
            findAllTopGenresQuery += " GROUP BY genre.GenreId";
            findAllTopGenresQuery += " HAVING totalNumber = ?";

            statement = connection.prepareStatement(findAllTopGenresQuery);
            //sets id to given id
            statement.setString(1, id);
            //sets the countNumber of the wanted genres to the genreCount found in previous query
            statement.setInt(2, genreCount);
            //executes query and saves results
            results = statement.executeQuery();

            //goes through results, creates GenreCountTotal objects and adds them to list
            while (results.next()) {
                String genreID = results.getString("GenreId");
                String genreName = results.getString("GenreName");
                int resultGenreCount = results.getInt("totalNumber");
                topGenre.add(new GenreCountTotal(genreID, genreName, resultGenreCount));
            }
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return topGenre;
    }
}
