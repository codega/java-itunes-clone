package com.experis.iTunesClone.data_access;

import com.experis.iTunesClone.models.Track;

import java.sql.*;
import java.util.ArrayList;

public class TrackData {
    private static Connection connection = null;

    public static ArrayList<Track> getFiveTracks(){
        //list to add five random tracks to
        ArrayList<Track> tracks = new ArrayList<Track>();

        try {
            //opens connection to database
            connection = DriverManager.getConnection(ConnectionHelper.URL);
            //SQL-statement to get five random tracks
            PreparedStatement statement = connection.prepareStatement("SELECT TrackId, Name FROM track ORDER BY RANDOM() LIMIT 5");
            //executes query and stores results
            ResultSet results = statement.executeQuery();
            //adds tracks to list
            while (results.next()){
                tracks.add(new Track(results.getString("TrackId"), results.getString("Name")));
            }
        } catch (SQLException exception){
            System.out.println(exception.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        return tracks;
    }

}
