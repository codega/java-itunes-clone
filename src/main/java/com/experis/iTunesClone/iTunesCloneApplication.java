package com.experis.iTunesClone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class iTunesCloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(iTunesCloneApplication.class, args);
	}

}
