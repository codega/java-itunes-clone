package com.experis.iTunesClone.controllers;

//Local imports
import com.experis.iTunesClone.data_access.TrackData;
import com.experis.iTunesClone.data_access.ArtistData;
import com.experis.iTunesClone.data_access.GenreData;
import com.experis.iTunesClone.data_access.TrackSearchData;
import com.experis.iTunesClone.models.Search;
import com.experis.iTunesClone.models.TrackFull;

//Spring imports
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class PageController {

    @GetMapping("/")
    //index-page, shows user recommendation
    public String index(Model model){
        model.addAttribute("tracks", TrackData.getFiveTracks());
        model.addAttribute("artists", ArtistData.getFiveArtists());
        model.addAttribute("genres", GenreData.getFiveGenres());
        model.addAttribute("search", new Search());
        return "index";
    }

    @GetMapping("/search")
    //Search-page, shows search results from user input on index-page
    public String search(Model model, Search search){
        ArrayList<TrackFull> searchResults = TrackSearchData.searchForTrack(search.getQuery());
        model.addAttribute("search", search.getQuery());

        //shows error-message "no results" if there's no results
        if (searchResults.size() == 0){
            model.addAttribute("nothing", true);
        } else {
            model.addAttribute("tracks", searchResults);
        }
        return "search";
    }

}
