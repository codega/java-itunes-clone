package com.experis.iTunesClone.controllers;

//local packages
import com.experis.iTunesClone.data_access.CustomerData;
import com.experis.iTunesClone.models.Customer;
import com.experis.iTunesClone.models.CustomerByCountry;
import com.experis.iTunesClone.models.CustomerByProfit;
import com.experis.iTunesClone.models.GenreCountTotal;

//spring packages
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
public class APIController {

    //Gets all customers in the database
    @GetMapping("/api/customers")
    public ArrayList<Customer> getAllCustomers(){
        return CustomerData.getAllCustomers();
    }

    //Posts new customer to database
    @PostMapping("/api/customers")
    public ResponseEntity newCustomer(@RequestBody Customer newCustomer){
        boolean created = CustomerData.addNewCustomer(newCustomer);
        if (created){
            return new ResponseEntity(HttpStatus.CREATED);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Updates a given customer in database
    @PutMapping("/api/customers/{id}")
    public ResponseEntity updateCustomer(@PathVariable String id, @RequestBody Customer customer){
        boolean created = CustomerData.updateExistingCustomer(id, customer);
        if (created){
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lists all countries by how many customers they have in the database, from most to least.
    @GetMapping("api/customers/countries/descending")
    public ArrayList<CustomerByCountry> getCustomersByCountriesDescending(){
        return CustomerData.getCustomersByCountriesDescending();
    }

    //Lists all customers by how much they spend, from most to least.
    @GetMapping("api/customers/profit/descending")
    public ArrayList<CustomerByProfit> getCustomersByProfitDescending(){
        return CustomerData.getCustomersByProfitDescending();
    }

    //Gets top genre(s) of a given user in the database
    @GetMapping("api/customers/{id}/genre")
    public ArrayList<GenreCountTotal> getUserTopGenre(@PathVariable String id){
        return CustomerData.getUserTopGenre(id);
    }
}
