package com.experis.iTunesClone.models;

public class Genre {
    private final String genreID;
    private final String genreName;

    public Genre(String genreID, String genreName){
        this.genreID = genreID;
        this.genreName = genreName;
    }

    public String getGenreID(){
        return genreID;
    }

    public String getGenreName() {
        return genreName;
    }
}
