package com.experis.iTunesClone.models;

public class CustomerByProfit {
    private final String customerID;
    private final String firstName;
    private final String lastName;
    private final double spending;

    public CustomerByProfit(String customerID, String firstName, String lastName, double spending) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.spending = spending;
    }

    public String getCustomerID() {
        return customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getSpending() {
        return spending;
    }
}
