package com.experis.iTunesClone.models;

public class GenreCountTotal {
    private final String genreID;
    private final String genreName;
    private final int genreTotal;

    public GenreCountTotal(String genreID, String genreName, int genreTotal){
        this.genreID = genreID;
        this.genreName = genreName;
        this.genreTotal = genreTotal;
    }

    public String getGenreID() {
        return genreID;
    }

    public String getGenreName() {
        return genreName;
    }

    public int getGenreTotal() {
        return genreTotal;
    }
}
