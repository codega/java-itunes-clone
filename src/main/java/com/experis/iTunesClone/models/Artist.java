package com.experis.iTunesClone.models;

public class Artist {
    private final String artistID;
    private final String artistName;

    public Artist(String artistID, String artistName){
        this.artistID = artistID;
        this.artistName = artistName;
    }

    public String getArtistID() {
        return artistID;
    }

    public String getArtistName() {
        return artistName;
    }
}
