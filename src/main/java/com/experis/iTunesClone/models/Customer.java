package com.experis.iTunesClone.models;

public class Customer {
    private final String customerID;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String country;
    private final String postalCode;
    private final String phoneNumber;

    public Customer(String customerID, String firstName, String lastName, String email, String country, String postalCode, String phoneNumber){
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
    }

    public String getCustomerID() {
        return customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {return email; }

    public String getCountry() {
        return country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
