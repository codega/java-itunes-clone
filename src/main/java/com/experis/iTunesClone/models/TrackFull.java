package com.experis.iTunesClone.models;

public class TrackFull {
    private final String trackName;
    private final String trackArtist;
    private final String trackAlbum;
    private final String trackGenre;

    public TrackFull(String trackName, String trackArtist, String trackAlbum, String trackGenre){
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackAlbum = trackAlbum;
        this.trackGenre = trackGenre;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public String getTrackAlbum() {
        return trackAlbum;
    }

    public String getTrackGenre() {
        return trackGenre;
    }
}
