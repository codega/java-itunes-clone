package com.experis.iTunesClone.models;

public class Track {
    private final String trackID;
    private final String trackName;

    public Track(String trackID, String trackName){
        this.trackID = trackID;
        this.trackName = trackName;
    }

    public String getTrackID() {
        return trackID;
    }

    public String getTrackName() {
        return trackName;
    }
}
