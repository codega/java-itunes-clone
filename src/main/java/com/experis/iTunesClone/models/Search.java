package com.experis.iTunesClone.models;

public class Search {
    private String query;

    public void setQuery(String query){
        this.query = query;
    }

    public String getQuery(){
        return query;
    }
}
