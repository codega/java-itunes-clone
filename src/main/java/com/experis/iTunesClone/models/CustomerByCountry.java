package com.experis.iTunesClone.models;

public class CustomerByCountry {
    private final String countryName;
    private final int customerCount;

    public CustomerByCountry(String countryName, int customerCount){
        this.countryName = countryName;
        this.customerCount = customerCount;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getCustomerCount() {
        return customerCount;
    }
}
