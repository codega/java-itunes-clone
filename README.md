# Java Task 5 - iTunes (Clone)

This is a simple website connected to the Chinook Music Store database. The website recommends the user 5 random songs, artists and genres. It also allows the user to search for songs in the database, to see more information about the songs. 

The application exposes information about the customer database. See below for working API endpoints. 

## Link to website

> https://experis-itunes-clone.herokuapp.com/

## API endpoints

> api/customers

GET-request: gets all customers in the database.

POST-request: adds a customer to the database.

New customers need a first name, last name, email, country, postalCode and phoneNumber.

> api/customers/[a customer ID]

PUT-request: updates an existing customer with new information. This needs all the aforementioned fields too.

> api/customers/[a customer ID]/genre

GET-request: gets the top genre(s) for an existing customer in the database.

> api/customers/profit/descending

GET-request: lists all customers by how much they spend, in descending order.

> api/customers/countries/descending

GET-request: lists all countries by how many customers they have, in descending order.